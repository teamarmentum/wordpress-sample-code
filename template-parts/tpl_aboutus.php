<?php
/**
 * Template Name: About Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?> 


    <div class="jumbotron about_us">
      <div class="container">
        <h2>Our Company</h2>
		<h4 class="text-nor">Holdfolio was founded on the principle of creating profitable partnerships.</h4>
        <div class="search_blk">
        	  <form name="searchemail" id="searchemail" method="post" action="<?php echo site_url('/register'); ?>">
        	<input type="text" name="email" id="email" placeholder="Enter your email address" />
            <input type="submit" value=" START INVESTING NOW " />
            </form>
        </div>
        <!-- // search_blk  -->   
      </div>
    </div>
    <!-- end jumbotron -->
	
      <div class="container">
        <!-- Example row of columns -->

        <div class="row">
            <div class="col-md-12 content_16 mrgnT30 mrgnB30">
           		<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
				<?php endwhile; ?>
				<?php endif; ?>
		
			
            </div>
        </div>

    </div> <!-- container  content-block -->


	<div class="container">
	
		<div class="image_block">
			<div class="row">
				<div class="col-md-6 col-sm-5 col-xs-12">
					<img src="<?php the_field('about_image');?>" alt="" class="img-big-width" />
				</div>
				
				<div class="col-md-6 col-sm-7 col-xs-12 image_blk_r">
					<div class="image_blk_content">
						<h3  class="title_20">  <?php the_field('about_side_title'); ?></h3>
						<?php the_field('about_side_content'); ?>
					</div>
				</div>
			</div>
			<!-- end row -->
		</div>
		<!-- // image_block -->
	
	</div>
	<!-- // container -->
	
	<div class="team_blk">
		<div class="container">
			<h2 class="title_h2_shadow"> <?php the_field('co-founder_title'); ?> </h2>
			<h4><?php the_field('co-founder_text'); ?></h4>
			
			<div class="row team_list ">
				<div class="col-md-offset-1  col-md-4 col-sm-5 col-xs-12">
					<figure>
					  <img src="<?php the_field('co-founder1'); ?>" alt="Jacob Blackett" class="img-circle">
					</figure>
					<div class="team_member">
						<h4><?php the_field('co-founder_1_title'); ?></h4>
						<p><?php the_field('co-founder_1_phone'); ?><br /> 
						<a href="mailto:<?php the_field('co-founder_1_email'); ?>"><?php the_field('co-founder_1_email'); ?></a></p>
						<?php /*<a href="<?php the_field('co-founder_text'); ?>" class="btn btn_brdr">
							<?php the_field('co-founder_1_button'); ?>
						</a> */ ?>
						
						<a href="javascript:void(0);" data-toggle="modal" data-target="#brief-model1" class="btn btn_brdr">
							<?php the_field('co-founder_1_button'); ?>
						</a>
						
			  
						  <div class="modal fade bs-example-modal-lg text-left" id="brief-model1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
							<div class="modal-dialog modal-md" role="document">
							  <div class="modal-content">
								<div class="row">
								  <div class="col-md-12 col-sm-12 col-xs-12 brief-content" style="padding:30px;">
										<h4>JACOB BLACKETT, CEO </h4>
										<p>Originally from Reno, Nevada, Jacob began his real estate career at a young age. In 2010, as a sophomore at the University of Nevada, Reno, when most of his contemporaries were immersed in college life, Jacob bought and sold his first two residential properties in Southern California. His drive, enthusiasm, and acumen for negotiating deals astounded his partners in the industry. </p>
<p>Once he made the move to Indianapolis, Jacob fell under the tutelage of Aaron Adams. Over the course of their partnership he amassed a lifetime’s worth of real estate knowledge, playing a role in the placement of over $40 million into single family rental properties. Using the partnerships he had forged during his time with Adams, Jacob set out with his trusted partner, Sterling White, to found Holdfolio. Holdfolio was founded on the basis of creating partnerships to profit from real estate. He created a low risk, high profit model that allows his clients to invest in pools of properties already held by his company.</p>
<p>At Holdfolio, Jacob primarily focuses on property acquisitions, financial planning, partner relations, and business development. Outside of his business, Jacob enjoys staying active, volunteering as a Big Brother, and education as a hobby. The book that helped set him on his entrepreneurial course is entitled, "Secrets of the Millionaire Mind," by T. Harv Eker.</p>
								  </div>
								</div>
							  </div>
							</div>
						 </div>	
					</div>
					<!-- end team_member --> 
				</div>
				
				<div class="col-md-offset-2  col-md-4 col-sm-5 col-xs-12">
					<figure>
					  <img src="<?php the_field('co-founder_2'); ?>" alt="Jacob Blackett" class="img-circle">
					</figure>
					<div class="team_member">
						<h4><?php the_field('co-founder_2_title'); ?></h4>
						<p><?php the_field('co-founder_2_phone'); ?><br /> 
						<a href="mailto:<?php the_field('co-founder_2_email'); ?>"><?php the_field('co-founder_2_email'); ?></a></p>
						<?php /* <a href="<?php the_field('co-founder_2_button_link'); ?>" class="btn btn_brdr">
							<?php the_field('co-founder_2_button'); ?>
						</a> */ ?>
						
						<a href="javascript:void(0);" data-toggle="modal" data-target="#brief-model2" class="btn btn_brdr">
							<?php the_field('co-founder_2_button'); ?>
						</a>
						
						<div class="modal fade bs-example-modal-lg text-left" id="brief-model2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
							<div class="modal-dialog modal-md" role="document">
							  <div class="modal-content">
								<div class="row">
								  <div class="col-md-12 col-sm-12 col-xs-12 brief-content" style="padding:30px;">
										<h4>STERLING WHITE, CMO</h4>
										<p>An Indianapolis native, Sterling has had an entrepreneurial mindset from a very young age. He has taken his natural energy and skill which began simply by selling homemade packets of drink flavoring to classmates at nine years of age, to co­founding an ever­expanding real estate company.</p>
<p>Much of the insight and trade knowledge he now possess, Sterling owes to his mentor Will Bates. The two formed a mastermind alliance and Bates imparted lessons from his years of experience managing well over five­hundred units in historic apartment complexes.</p>
<p>At Holdfolio, Sterling focuses his time on building strong investor relations with clients, property acquisitions, and creating strategic and directed marketing campaigns to grow their brand. Sterling is hands on about creating value for his investors, and he truly believes in the role that he and his company play in establishing a path to financial freedom for those under his watch.</p>
<p>In his spare time Sterling enjoys learning anyway he can. One of his biggest professional influences is Tony Robbins, and his book “Awaken the Giant Within” is a cornerstone of his personal philosophy. In the spirit of giving back and community, Sterling is also participating in the Guinness World Record for Fastest Fireman Carry, where the proceeds are being donated to the Little Red Door Cancer Agency. Sterling also serves as a Big Brother in Indianapolis.</p>
								  </div>
								</div>
							  </div>
							</div>
						 </div>
					</div>
					<!-- end team_member --> 
					
				</div>
			</div>
		</div>
	</div>
	<!--  end team_blk -->
	
	<div class="container">
	
		<div class="image_block">
			<div class="row">
				<div class="col-md-6 col-sm-5 col-xs-12">
					<img src="<?php the_field('step_1_image'); ?>" alt="" class="img-big-width" />
				</div>
				
				<div class="col-md-6 col-sm-7 col-xs-12 image_blk_r">
					<div class="image_blk_content">
						<h3 class="title_20"><?php the_field('step_1_title'); ?></h3>
						<?php the_field('step_1_content'); ?>
						
					</div>
				</div>
			</div>
			<!-- end row -->
		</div>
		<!-- // image_block -->
		
		<div class="image_block">
			<div class="row">
				<div class="col-md-6 col-sm-5 col-xs-12">
					<img src="<?php the_field('step_2_image'); ?>" alt="" class="img-big-width" />
				</div>
				
				<div class="col-md-6 col-sm-7 col-xs-12 image_blk_r">
					<div class="image_blk_content">
						<h3  class="title_20"><?php the_field('step_2_title'); ?></h3>
						
						<h4><?php the_field('step_2_jb_realty'); ?></h4>
						<p>	<?php the_field('step_2_jb_realty_content'); ?></p>
						
						<h4><?php the_field('step_2_jb_management'); ?></h4>
						<p><?php the_field('step_2_jb_management_content'); ?></p>
						
						<h4><?php the_field('step_2_jb_holding'); ?></h4>
						<p><?php the_field('step_2_jb_holding_content'); ?></p>
						

					</div>
				</div>
			</div>
			<!-- end row -->
		</div>
		<!-- // image_block -->
		
		<div class="image_block">
			<div class="row">
				<div class="col-md-6 col-sm-5 col-xs-12">
					<img src="<?php the_field('step_3_image'); ?>" alt="" class="img-big-width" />
				</div>
				
				<div class="col-md-6 col-sm-7 col-xs-12 image_blk_r">
					<div class="image_blk_content">
						<h3  class="title_20"><?php the_field('step_3_title'); ?></h3>
						<div class="content_16 indec">
						<?php the_field('step_3_content'); ?>
					</div>
					</div>
				</div>
			</div>
			<!-- end row -->
		</div>
		<!-- // image_block -->
	<hr class="mrgnT30 mrgnB30" />
	</div>
	<!-- // container -->
	
	
	
	<div class="container content_16 mrgnB30">
		
		<h2 class="text-center text-nor mrgnB30"><strong><?php the_field('about_last_content_title'); ?></strong></h2>
		<?php the_field('about_last_content'); ?>
	
	</div>
	

<?php
get_footer();?>
	

  <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <script>
  
  // When the browser is ready...
  $(function() {
  
    // Setup form validation on the #register-form element
    $("#searchemail").validate({
    
        // Specify the validation rules
        rules: {
         
            email: {
                required: true,
                email: true
            },
          
        },
        
        // Specify the validation error messages
        messages: {
            	email:{required: "Please Enter Email",email:"Please Enter Valid Email"},
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>