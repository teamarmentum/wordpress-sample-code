<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
//if($user_ID==0){ wp_redirect( home_url('home'));}
get_header( 'shop' ); ?>


<?php 
	$args = array(
    'orderby'    => 'id',
    'order'      => 'ASC',
    'hide_empty' => true,
    'include'    => $ids
);
$product_categories = get_terms( 'product_cat', $args );
$count = count($product_categories);



if ($count>0){
  $total_count = count($product_categories);
  $i=0;
  $nav_count=0;
  $nav_details = array();
  
   foreach ( $product_categories as $product_category )
	{
	$i++;
	
	
	$nav_details[$i] = array('name'=>$product_category->name,'slug'=>$product_category->slug,'icon'=>$fa_icon);
	}
    foreach ( $product_categories as $product_category )
	{
	$nav_count++;
	 
	 
	 
	  $args = array(
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'slug',
                    // 'terms' => 'white-wines'
                    'terms' => $product_category->slug
                )
            ),
            'post_type' => 'product',
            'orderby' => 'title,'
        );
	$products = new WP_Query( $args );
if ( $products->have_posts() ) {
  
  
  
  
?>

<div class="cat_products <?php echo $product_category->slug;?> <?php echo $class;?>" id="catp-<?php echo $product_category->slug;?>">
  <div class="container ">
    <div class="row">
      <div class="col-md-12 content_16">
        <h2 class="title_group border-blue-title"> <span><?php echo $product_category->name;?></span>
          <?php //print_r($nav_details);
          
          
	
	
          if($total_count==$nav_count){
            
            $nav_name = $nav_details[$nav_count-1]['name'];
            $nav_slug = $nav_details[$nav_count-1]['slug'];
	   $fa_icon="fa fa-caret-up";
          }else{
             $nav_name = $nav_details[$nav_count+1]['name'];
            $nav_slug = $nav_details[$nav_count+1]['slug'];
         $fa_icon="fa fa-caret-down";
            
          }
          
          ?><div class="asce_desce"> <a href="#catp-<?php echo $nav_slug;?>" class="scoll-smooth"><?php echo $nav_name;?> <br>
              <i class="<?php echo $fa_icon;?>" aria-hidden="true"></i> </a> </div>
        </h2>
        
       <?php  if($product_category->slug=='past-opportunities'){ ?> 
       <div class="image_block investment_grid_view">
       <div class="row">   
        <?php } ?>
        
        <?php 
	    while ( $products->have_posts() ) {
            $products->the_post();
			$pro = wc_get_product( $post->ID );
			$src = $pro->get_image('default',array("class"=>"img-big-width"));
			$src_past = $pro->get_image('default',array("class"=>"img-responsive"));
			$address = $pro->get_attribute( 'address' );
			$minimum_investment = $pro->get_attribute( 'minimum-investment' );
			$irr_from = $product->get_attribute( 'irr-from' );
			$irr_to = $product->get_attribute( 'irr-to' );
			$no_properties= $product->get_attribute( 'no-of-properties' );
		    $property_type= $product->get_attribute( 'property-type' ); 
			$property_term = $pro->get_attribute( 'property-term' );
			$estimated_market_value = $pro->get_attribute( 'estimated-fair-market-value' );
			$detail_url = get_permalink( $post->ID );
            ?>
         <?php  if($product_category->slug!='past-opportunities'){ ?>    
        <div class="image_block investment_list_view">
          <div class="row">
            <div class="col-md-6 col-sm-5 col-xs-12"><?php echo $src;?> </div>
            <div class="col-md-6 col-sm-7 col-xs-12 image_blk_r">
              <div class="investment_content image_blk_content">
                <div class="invest_title_group">
                  <h3>
                    <?php the_title(); ?>
                  </h3>
                  <p><?php echo $address;?></p>
                </div>
                <!-- // end invest_title_group -->
                
                <div class="invest_brif">
                  <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-12 brder_r">
                      <label>Cash on Cash Return</label>
                      <span class="label_value"><?php echo $pro->get_attribute( 'ccr_from' ); ?>% - <?php echo $pro->get_attribute( 'ccr_to' ); ?>%</span> </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 brder_r">
                      <label>Min. Investment</label>
                      <span class="label_value"><?php hold_price($minimum_investment);?></span> </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 brder_r">
                      <label>IRR</label>
                      <span class="label_value"><?php hold_percentage($irr_from) ?> - <?php hold_percentage($irr_to) ?></span> </div>
                  </div>
                  <!--// end row --> 
                </div>
                <!-- end invest_brif -->
                
                <div class="invest_details">
                  <p>No. of Properties  ...................................................... <?php echo $no_properties;?></p>
                  <p>Security Type  ...................................................... <?php echo $property_type;?></p>
                  
                  
              <?php 
              
              if($product_category->slug=='upcoming'){
              $check_inv=check_invested_prop($user_id,$pro->id);
					if(count($check_inv)>0)
					{		?>
              	<p> <span class="btn btn-readmore">Reserved</span>   <a href="#" class="btn btn-readmore reserve_spot" id="reservspot_<?php echo get_the_ID(); ?>" onclick="$('#myModalReserv_<?php echo get_the_ID(); ?>').modal('show');">Change</a> </p>
					   <?php
					   
					}else{
              ?>
              	<p> <a href="#" class="btn btn-readmore reserve_spot" id="reservspot_<?php echo get_the_ID(); ?>" onclick="$('#myModalReserv_<?php echo get_the_ID(); ?>').modal('show');">Reserve My Spot</a> </p>
          <?php } ?>
              <?php }else{ ?>
              	<p> <a href="<?php echo $detail_url;?>" class="btn btn-readmore">More Details</a> </p>
               <?php } ?>
             <?php 	if(count($check_inv)>0) { ?>
              <div class="modal fade bs-example-modal-lg" id="myModalReserv_<?php echo get_the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="step_content">
                          <div class="step_content_area">
                            <h3>Reserve My Spot</h3>
                            <div id="reserve_response"></div>
                            <form method="post" id="reserv_spot_from_<?php echo get_the_ID(); ?>" class="reserv_spot_from comment-form form" novalidate="novalidate">
                              <input type="hidden" name="serv_root" id="serv_root" value="<?php echo get_site_url();?>">
                                  <input type="hidden" name="minvsest" id="minvsest" value="<?php echo $minimum_investment;?>">
                                  <input type="hidden" name="pro_id" id="pro_id" value="<?php echo $product->id;?>">
                                  <input type="hidden" name="order_id" id="order_id" value="<?php echo $check_inv->ID;?>">
                              
                              
                              <div class="min_investment"> <span>Min Investment:   $<?php echo $minimum_investment;?></span> </div>
                              <div class="comment-investment-amount" >
                                <label for="author"><strong>Investment Amount</strong></label>
                                <div class="input-group"> <span class="input-group-addon">$</span>
                                  <input id="inv_amount" class="calculate_investment" name="reserve_amount" required="required" title="Please Enter Investment Amount" type="text" value="<?php echo get_post_meta($check_inv->ID,'_order_total',true); ?>" size="30" aria-required="true" data-ccr-value="<?php echo get_ccr_value_by_id($product->id); ?>">
                                </div>
                              </div>
                             <p class="form-estimated-quaterly text-center content_16"> Estimated Quaterly Cash Flow: <br>
            $<?php 
           echo '<span class="total_estimated_value_html">'.get_post_meta($check_inv->ID,'_total_estimated_value',true).'</span>';
            
            //echo round($esti_cash,2);?> </p>
        <input type="hidden" name="ccr" class="ccr_value" value="<?php echo $ccr; ?>" / >
        <input type="hidden" name="total_estimated_value" class="total_estimated_value" value="0" / >
                              <div class="clearfix"> </div>
                              <div class="submit_btn form text-center mrgnT15">
                                <button class="submit btn validate_minimum_amount" type="submit" data-min-amount="<?php echo $minimum_investment;?>">Update</button>
                              </div>
                            </form>
                          </div>
                          
                          <!-- // end contact_question --> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
             <?php }else{ ?>
              <div class="modal fade bs-example-modal-lg" id="myModalReserv_<?php echo get_the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-md" role="document">
                  <div class="modal-content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="step_content">
                          <div class="step_content_area">
                            <h3>Reserve My Spot</h3>
                            <div id="reserve_response"></div>
                            <form method="post" id="reserv_spot_from_<?php echo get_the_ID(); ?>" class="reserv_spot_from comment-form form" novalidate="novalidate">
                              <input type="hidden" name="serv_root" id="serv_root" value="<?php echo get_site_url();?>">
                                  <input type="hidden" name="minvsest" id="minvsest" value="<?php echo $minimum_investment;?>">
                                  <input type="hidden" name="pro_id" id="pro_id" value="<?php echo $product->id;?>">
                              
                              
                              <div class="min_investment"> <span>Min Investment:   $<?php echo $minimum_investment;?></span> </div>
                              <div class="comment-investment-amount" >
                                <label for="author"><strong>Investment Amount</strong></label>
                                <div class="input-group"> <span class="input-group-addon">$</span>
                                  <input id="inv_amount" class="calculate_investment" name="reserve_amount" required="required" title="Please Enter Investment Amount" type="text" value="" size="30" aria-required="true" data-ccr-value="<?php echo get_ccr_value_by_id($product->id); ?>">
                                </div>
                              </div>
                             <p class="form-estimated-quaterly text-center content_16"> Estimated Quaterly Cash Flow: <br>
            $<?php 
           echo '<span class="total_estimated_value_html">0</span>';
            
            //echo round($esti_cash,2);?> </p>
        <input type="hidden" name="ccr" class="ccr_value" value="<?php echo $ccr; ?>" / >
        <input type="hidden" name="total_estimated_value" class="total_estimated_value" value="0" / >
                              <div class="clearfix"> </div>
                              <div class="submit_btn form text-center mrgnT15">
                                <button class="submit btn validate_minimum_amount" type="submit" data-min-amount="<?php echo $minimum_investment;?>">Reserve</button>
                              </div>
                            </form>
                          </div>
                          
                          <!-- // end contact_question --> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
             <?php  } ?>
             
              <div class="clearfix"> </div>
                  
                </div>
                <!-- end invest_details --> 
                
              </div>
            </div>
          </div>
          <!-- end row --> 
        </div>
        <?php }else{ ?>
        
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="image_block"> <?php echo $src_past;?>
            <div class="img_content_blk">
              <label><?php echo $address;?></label>
              <span><?php //echo $property_term;?>$<?php echo ($estimated_market_value) ? number_format($estimated_market_value) : '-';;?></span> </div>
          </div>
          <div class="investment_content">
            <div class="invest_title_group">
              <h3><?php the_title(); ?></h3>
            </div>
            <!-- // end invest_title_group -->
            
            <div class="invest_brif">
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 brder_r">
                  <label>Cash on Cash Return</label>
                      <span class="label_value"><?php echo $pro->get_attribute( 'ccr_from' ); ?>% - <?php echo $pro->get_attribute( 'ccr_to' ); ?>%</span> </div>
                <div class="col-md-4 col-sm-4 col-xs-12 brder_r">
                  <label>Min. Investment</label>
                  <span class="label_value">$<?php echo ($minimum_investment) ? number_format($minimum_investment) : '-';?></span> </div>
                <div class="col-md-4 col-sm-4 col-xs-12 brder_r">
                  <label>IRR</label>
                  <span class="label_value"><?php echo $irr_from;?>% - <?php echo $irr_to;?>%</span> </div>
              </div>
              <!--// end row --> 
            </div>
            <!-- end invest_brif -->
            
            <div class="invest_details">
              <!--<p>Property Type  .................... <?php echo $no_properties;?></p>-->
              <!--<p>Hold Period .................... 3-5 Years</p>-->
              <p class="dotted_styles">Number of Properties  ..................................................................<span><?php echo $no_properties;?></span></p>
              <p class="dotted_styles">Term ......................................................................................<span><?php echo ($pro->get_attribute( 'property-term' )) ? $pro->get_attribute( 'property-term' ) : ''; ?></span></p>
            </div>
            <!-- end invest_details --> 
            
          </div>
        </div>
        
        <?php } ?>
        <?php } ?>
          <?php  if($product_category->slug=='past-opportunities'){ ?> 
       </div> 
        </div>  
 
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php } ?>
<?php } ?>
<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>
<?php get_footer( 'shop' ); ?>
